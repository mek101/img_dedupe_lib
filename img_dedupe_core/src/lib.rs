mod filter;
mod graphconstructor;
mod utils;
mod wrappers;

use std::num::NonZeroUsize;
use std::path::Path;
use std::sync::Arc;

use img_hash::HashAlg;
use petgraph::{Graph, Undirected};
use rayon::{ThreadPool, ThreadPoolBuilder};

use crate::wrappers::cachemanager::{CacheManager, CacheMode};
use crate::wrappers::filedata::FileData;

// Public exports
pub use crate::wrappers::Distance;
pub use crate::utils::WouldOverflow;

pub type ImageGraph = Graph<Arc<FileData>, Distance, Undirected>;

/// A handler to keep and re-use resources that would need to be re-initialized every time otherwise.
pub struct Deduper {
    pool: ThreadPool,
    cache_manager: Arc<CacheManager>,
}

impl Deduper {
    pub fn new(parallelism: NonZeroUsize, cache_path: &Path, cache_mode: CacheMode) -> Self {
        Self {
            pool: ThreadPoolBuilder::new()
                .num_threads(parallelism.get())
                .build()
                .unwrap(),
            cache_manager: Arc::new(CacheManager::new(cache_path, cache_mode)),
        }
    }

    pub fn graph_images<'a>(
        &self,
        images: impl IntoIterator<Item = &'a Arc<FileData>> + Send,
        hash_alg: HashAlg,
    ) -> Result<Box<ImageGraph>, WouldOverflow> {
        graphconstructor::build_graph(images, hash_alg, &self.cache_manager, &self.pool)
    }

    pub fn split_image_graph(
        &self,
        graph: &ImageGraph,
        tolerance_threshold: u32,
        discard_unique: bool,
    ) -> Vec<Vec<Arc<FileData>>> {
        filter::split_to_groups(graph, tolerance_threshold, discard_unique, &self.pool)
    }

    pub fn rank<P>(
        &self,
        images: &[Vec<Arc<FileData>>],
        predicates: &[P],
    ) -> Vec<Vec<(Arc<FileData>, i32)>>
    where
        P: Sync + Fn(&FileData) -> i32 + Send,
    {
        filter::rank(images, predicates, &self.pool)
    }

    pub fn dedupe_images<'a, P>(
        &self,
        images: impl IntoIterator<Item = &'a Arc<FileData>> + Send,
        hash_alg: HashAlg,
        tollerance_threshold: u32,
        discard_unique: bool,
        predicates: &[P],
    ) -> Result<Vec<Vec<(Arc<FileData>, i32)>>, WouldOverflow>
    where
        P: Sync + Fn(&FileData) -> i32 + Send,
    {
        let graph = self.graph_images(images, hash_alg)?;
        let groups = self.split_image_graph(graph.as_ref(), tollerance_threshold, discard_unique);
        Ok(self.rank(&groups, predicates))
    }
}
