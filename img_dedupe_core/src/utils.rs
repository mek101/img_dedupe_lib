use std::error::Error;
use std::fmt::{Display, Result};

#[derive(Debug)]
pub struct WouldOverflow(pub usize, pub usize);

impl Error for WouldOverflow {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        None
    }

    fn description(&self) -> &str {
        "description() is deprecated; use Display"
    }

    fn cause(&self) -> Option<&dyn Error> {
        self.source()
    }
}

impl Display for WouldOverflow {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result {
        write!(f, "({}, {})", self.0, self.1)
    }
}

/// Obtains the number of undirected edges for a graph with the given number of nodes.
pub(crate) fn get_edge_count(n: usize) -> usize {
    n * (n - 1) / 2
}

#[cfg(test)]
pub(crate) mod tests {
    use std::fs;
    use std::path::PathBuf;
    use std::sync::Arc;

    use glob;
    use img_hash::HashAlg;
    use num_cpus;
    use once_cell::sync::Lazy;
    use rayon::{ThreadPool, ThreadPoolBuilder};

    use crate::wrappers::cachemanager::{CacheManager, CacheMode};
    use crate::wrappers::filedata::FileData;

    /// Path to the directory containing the images for the tests.
    const TEST_IMAGES_PATH: Lazy<PathBuf> = Lazy::new(|| {
        let mut path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        path.push("../resources/tests/images/");
        path
    });

    /// The number of unique images (and therefore groups).
    pub const TEST_IMAGES_UNIQUE_COUNT: usize = 2;

    /// Vector with the groups of images that should be idientifies as duplicates.
    pub const TEST_IMAGES_CORRECT_MATCHES: Lazy<Vec<Vec<PathBuf>>> = Lazy::new(|| {
        // All the groups basenames as patterns.
        let groups_basenames = vec![
            "ani_girl_1*",
            "fist_*",
            "landscape_1*",
            "landscape_4*",
            "unique_1.jpg",
            "unique_2.jpg",
        ];

        // Collects each group in it's own vector.
        let mut groups = groups_basenames
            .iter()
            .map(|g| {
                let mut p = TEST_IMAGES_PATH.clone();
                p.push(g);
                let pattern = p.to_str().unwrap();

                let mut group = glob::glob(pattern)
                    .unwrap()
                    .map(|r| r.unwrap())
                    .collect::<Vec<PathBuf>>();
                group.shrink_to_fit();
                group
            })
            .collect::<Vec<Vec<PathBuf>>>();
        groups.shrink_to_fit();
        groups
    });

    /// Image files to test on.
    pub const TEST_FILEDATA: Lazy<Vec<Arc<FileData>>> = Lazy::new(|| {
        fs::read_dir(TEST_IMAGES_PATH.as_path())
            .unwrap()
            .filter_map(|p| {
                let path = p.unwrap().path();
                let img_res = image::open(path.clone());
                match img_res {
                    Ok(img) => Some(Arc::new(FileData::new(path.as_path(), img))),
                    Err(_) => None,
                }
            })
            .collect()
    });

    pub const TEST_CACHE_MANAGER_NONE: Lazy<CacheManager> =
        Lazy::new(|| CacheManager::new(PathBuf::from("").as_path(), CacheMode::None));

    pub const TEST_POOL: Lazy<ThreadPool> = Lazy::new(|| {
        let threads = num_cpus::get() + 1;
        ThreadPoolBuilder::new()
            .num_threads(threads)
            .build()
            .unwrap()
    });

    /// The hashing algorithm for testing.
    pub const TEST_HASH_ALG: HashAlg = HashAlg::DoubleGradient;

    /// The default tolerance threshold for testing.
    pub const TEST_TOLERANCE_THRESHOLD: u32 = 10;
}
