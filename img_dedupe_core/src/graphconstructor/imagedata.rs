use std::sync::Arc;

use img_hash::{Hasher, ImageHash};
use once_cell::sync::OnceCell;
use petgraph::graph::NodeIndex;

use crate::wrappers::cachemanager::CacheConnector;
use crate::wrappers::filedata::FileData;
use crate::ImageGraph;

/// Data relative to an image.
pub struct ImageData {
    filedata: Arc<FileData>,
    signature: OnceCell<ImageHash>,
    node_index: OnceCell<NodeIndex<u32>>,
}

impl ImageData {
    fn get_signature(&self, hasher: &Hasher, cache: &CacheConnector) -> &ImageHash {
        self.signature.get_or_init(&|| {
            // First thing, query the cache.
            if let Some(cache_signature) = cache.get_signature(&self.filedata) {
                cache_signature
            } else {
                // If the value isn't cached, generate it.
                let signature = hasher.hash_image(self.filedata.image_impl());
                // Update the cache.
                cache.set_signature(&self.filedata, &signature);
                signature
            }
        })
    }

    pub fn new(filedata: Arc<FileData>) -> Self {
        Self {
            filedata,
            signature: OnceCell::new(),
            node_index: OnceCell::new(),
        }
    }

    // TODO move this where it makes sense.
    /// Ensures a node is inserted in the graph, annd returns the index of the node.
    pub fn insert_or_get_node(&self, graph: &mut ImageGraph) -> NodeIndex<u32> {
        if let Some(index) = self.node_index.get() {
            *index
        } else {
            let index = graph.add_node(self.filedata.clone());
            self.node_index.get_or_init(&|| index);
            index
        }
    }

    /// The hamming distance between 2 images.
    pub fn get_distance(&self, other: &ImageData, hasher: &Hasher, cache: &CacheConnector) -> u32 {
        let sign = self.get_signature(&hasher, &cache);
        let other_sign = other.get_signature(&hasher, &cache);

        sign.dist(&other_sign)
    }
}
