# Image Deduplication Library
An image deduplication library written in rust, with configurable cache, multi-threading and filtering support.

It contains the tools to:

- Create graphs with images as nodes and edges as a quantified similarity rating.

- Partition the above graphs into groups of duplicate images by cutting the edges under a threshold given as input.

- Rank the duplicate images inside those groups by their characteristics, metadata and path, with a customizable set of predicates, in order to easily select which to keep.